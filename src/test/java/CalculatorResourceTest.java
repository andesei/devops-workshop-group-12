//import jdk.jfr.Description;
import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";

        assertEquals(400, calculatorResource.calculate(expression));

        expression = " 300 - 99 ";
        assertEquals(201, calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+99";
        assertEquals(399, calculatorResource.sum(expression));

        expression = "300+100+500";
        assertEquals(900, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-2";
        assertEquals(18, calculatorResource.subtraction(expression));

        expression = "20-10-5";
        assertEquals(5, calculatorResource.subtraction(expression));
    }

    @Test
    //@Description("This method will test the multiplication method")
    public void testMultiplication(){

        CalculatorResource calculatorResource = new CalculatorResource();
        String equation = "4*6";
        assertEquals(24, calculatorResource.multiplication(equation));

        equation = "2*2*2";
        assertEquals(8, calculatorResource.multiplication(equation));
    }

    @Test
    //@Description("This method will test the division method")
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String equation = "20/2";
        String equation1 = "36/6";

        assertEquals(10, calculatorResource.division(equation));
        assertEquals(6, calculatorResource.division(equation1));

        equation = "8/2/2";
        equation1 = "16/2/2";
        assertEquals(2, calculatorResource.division(equation));
        assertEquals(4, calculatorResource.division(equation1));
    }
}
